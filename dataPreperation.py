import pandas as pd
import numpy as np
import cv2
from keras.preprocessing import image
import matplotlib.pyplot as plt
import os

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
eyePair_cascade = cv2.CascadeClassifier('haarcascade_mcs_eyepair_big.xml')

def return_eye_pair(infile, outfile):
    img = cv2.imread(infile)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


    faces = face_cascade.detectMultiScale(gray, 1.1, 3)
    if len(faces) == 0: return
    for x,y,w,h in faces:
    	roi_gray = gray[y:y+h, x:x+w]
    	roi_color = img[y:y+h, x:x+w]
    	eyes = eyePair_cascade.detectMultiScale(roi_gray)	
    	if len(eyes) == 0: return	
    	for (ex,ey,ew,eh) in eyes:
    		eyes_roi = roi_color[ey: ey+eh, ex:ex + ew]
			#cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

    cv2.imwrite(outfile, eyes_roi)
	
	
    
def load_images_from_folder(folder):
    outFolder = 'images'
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            return_eye_pair(os.path.join(folder,filename), os.path.join(outFolder,filename))
            

#Read csv labels and 500
df = pd.read_csv('labels.csv')
df.drop(columns="user.id", inplace=True)
print(df)

df = pd.read_csv('500_pictz_satz.csv')
print(df)


def main():

    folder = 'images_org'
    load_images_from_folder(folder)

 
if __name__ == '__main__':
    main()